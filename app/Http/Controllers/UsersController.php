<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller as AppController;
use \App\Models as Database;

class UsersController extends AppController
{
    /**
     * Constructor
     */
    public function __construct() {
        $this->middleware('auth');
    }

    /**
     * List user
     *
     * /users
     */
    public function index()
    {
        $users = Database\User::with('role')->get();

        return view('users.index', compact('users'));
    }

    /**
     * Create new user
     *
     * GET users/create
     */
    public function create()
    {
        return view('users.create');
    }

    /**
     * Edit user
     *
     * GET users/{id}/edit
     *
     * @param Database\User $user
     */
    public function edit(Database\User $user)
    {
        return view('users.edit', compact(
            'user'
        ));
    }
}
